#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <DHT.h>
#include <ESP8266WiFi.h>
#include <Firebase_ESP_Client.h>

#include "addons/TokenHelper.h"
#include "addons/RTDBHelper.h"

#define I2C_ADDR 0x27
#define WIFI_SSID "MERCUSYS_DA07"
#define WIFI_PASSWORD "camera321"

#define API_KEY "AIzaSyC_Ym5LtD3Hu3ZFnhJmU_7lyg3aaU3Mcn8"
#define DATABASE_URL "weather-simulation-sistem-default-rtdb.europe-west1.firebasedatabase.app"


LiquidCrystal_I2C lcd(I2C_ADDR, 16, 2);
const int lightSensorPin = A0;
const int rainSensorPin = D3;
#define DHTPIN D4
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

int scaledLightValue = 0;
String rainStatus = "";
int temperature = 0;
int humidity = 0;

FirebaseData fbdo;
FirebaseAuth auth;
FirebaseConfig config;

void setup() {
  Serial.begin(115200);
  lcd.init(); 
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Initializing LCD...");
  Serial.println("LCD initialized");

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Connecting to WiFi");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("\nConnected to WiFi");

  config.api_key = API_KEY;
  config.database_url = DATABASE_URL;
  config.token_status_callback = tokenStatusCallback;

  Firebase.begin(&config, &auth);
  Firebase.reconnectWiFi(true);
  Serial.println("Firebase initialized");

  dht.begin();
  Serial.println("DHT sensor initialized");

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("LCD Test OK");
  delay(2000);
}

void loop() {

  int lightValue = analogRead(lightSensorPin);
  int rainValue = digitalRead(rainSensorPin);
  int temperature = dht.readTemperature();
  int humidity = dht.readHumidity();


  scaledLightValue = map(lightValue, 0, 1024, 100, 0);

  rainStatus = (rainValue == LOW) ? "WET" : "DRY";

  // Debug messages for sensor values
  Serial.print("Light: ");
  Serial.println(scaledLightValue);
  Serial.print("Rain: ");
  Serial.println(rainStatus);
  Serial.print("Temperature: ");
  Serial.println(temperature);
  Serial.print("Humidity: ");
  Serial.println(humidity);

 
  if (Firebase.RTDB.setInt(&fbdo, "/Light", scaledLightValue) &&
      Firebase.RTDB.setString(&fbdo, "/Rain", rainStatus) &&
      Firebase.RTDB.setInt(&fbdo, "/Temperature", temperature) &&
      Firebase.RTDB.setInt(&fbdo, "/Humidity", humidity)) {
    Serial.println("Data sent to Firebase successfully");

   
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("L:");
    lcd.print(scaledLightValue);
    lcd.print("%   ");

    lcd.setCursor(9, 0);
    lcd.print("R:");
    lcd.print(rainStatus);
    lcd.print("   ");

    lcd.setCursor(0, 1);
    lcd.print("T:");
    lcd.print(temperature);
    lcd.print("C  ");

    lcd.setCursor(9, 1);
    lcd.print("H:");
    lcd.print(humidity);
    lcd.print("%   ");
  } else {
    Serial.println("Failed to send data to Firebase");
    Serial.println("Reason: " + fbdo.errorReason());
  }

  delay(2000); 
}
