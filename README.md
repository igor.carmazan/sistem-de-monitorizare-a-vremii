# Sistem de monitorizare a vremii
# Contine 2 fisiere cu cod, Arduino, HTML
# Adresa repository: https://gitlab.upt.ro/igor.carmazan/sistem-de-monitorizare-a-vremii.git
# Pasii necesari pentru compilarea aplicatie:
    1. Conectarea senzorilor si ecranului cu placa de dezvoltare ESP8266
    2. Descarcarea Arduino IDE, instalarea bibliotecilor.
    3. Modificarea credentialelor pentru reteaua Wi-fi cat si pentru Firebase.
    4. Incarcarea codului pe placa de dezvoltare
    5. Schimbarea credentialelor in fisierul HTML
